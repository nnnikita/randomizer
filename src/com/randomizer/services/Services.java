package com.randomizer.services;

import java.util.Random;
import java.util.Scanner;

public class Services {
    private static final String HELP = "help";
    private static final String EXIT = "exit";
    private static final String GENERATE = "generate";
    private static final String INT_MIN_NUM = "Enter the minimum number of the range(from 1 to 500):";
    private static final String INT_MIN_MAX = "Enter the maximum number of the range(from ";
    private static final String PROMPT = "Input <exit> to close program or <generate> to generate new random number:";
    private static final String INCORRECT_COMMAND = "WRONG COMMAND! Please, input <<generate>>, <<exit>> or <<help>>";
    private static final String INCORRECT_NUM = "Number out of range! Please input correct numbers!";
    private static final String GOODBYE = "Program is closed. Goodbye!";

    private static Scanner scanner = new Scanner(System.in);
    private static Random random = new Random();

    int min;
    int max;
    int countElem;
    int[] myArray;
    int countGenerates = 0;
//    String command;


    public void inputMinMaxNum() {
        System.out.println(INT_MIN_NUM);
        min = scanner.nextInt();
        if (min < 1 || min > 500) {
            System.out.println(INCORRECT_NUM);
            inputMinMaxNum();
        }
        System.out.println(INT_MIN_MAX + min + " to 500):");
        max = scanner.nextInt();
        if (max < min || max > 500) {
            System.out.println(INCORRECT_NUM);
            inputMinMaxNum();
        }
    }

    public void initializeArray() {
        countElem = max - min + 1;      //количество элементов массива
        myArray = new int[countElem];     //Инициализируем массив на посчитаное количество элементов
        for (int i = 0; i < countElem; i++) {       //Заполняем массив числами в нашем диапазоне
            myArray[i] = min + i;
        }
        int n = myArray.length;             //Перемешиваем наш массив чисел в случайном порядке
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(myArray, i, change);
        }
    }

    public void launchInterface() {
            System.out.println("Input command:");
            String command = scanner.next();
            switch (command) {
                case GENERATE:
                    System.out.println("Your random number is: " + myArray[countGenerates]);
                    if (countGenerates == (myArray.length - 1)) {
                        System.out.println("It was last number of your range. " + GOODBYE);
                        System.exit(0);
                    }
                    countGenerates++;
                    break;
                case HELP:
                    System.out.println(PROMPT);
                    break;
                case EXIT:
                    System.out.println(GOODBYE);
                    System.exit(0);
                default:
                    System.out.println(INCORRECT_COMMAND);
                    break;
            }
        }

    private static void swap(int[] myArray, int i, int change) {
        int temp = myArray[i];
        myArray[i] = myArray[change];
        myArray[change] = temp;
    }
}