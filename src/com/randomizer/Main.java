package com.randomizer;

import com.randomizer.services.Services;

public class Main {

    public static void main(String[] args) {

        Services services = new Services();

        services.inputMinMaxNum();
        services.initializeArray();
        while (true) {
            services.launchInterface();
        }
    }
}
